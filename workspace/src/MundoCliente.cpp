// Mundo.cpp: implementation of the CMundo class.
// Autor: Kendra Gabriela Romero Gutierrez
//////////////////////////////////////////////////////////////////////


#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <fcntl.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <iostream>

char *org;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	datosm.esfera=esfera;
	datosm.raqueta1=jugador1;
	datosm.accion=0;
	datosm2.esfera=esfera;
	datosm2.raqueta1=jugador1;
	datosm2.accion=0;
	
	disparos1.setPos(-300, -300);
	disparos1.setRadio(0);
	disparos1.setVel(0, 0);
	disparos2.setPos(-300, -300);
	disparos2.setRadio(0);
	disparos2.setVel(0, 0);
	Init();
}

CMundo::~CMundo()
{
	munmap(datosmpunt,sizeof(DatosMemCompartida));
	munmap(datosmpunt2,sizeof(DatosMemCompartida));
	unlink("Datos_Compartidos2.txt");
	unlink("Datos_Compartidos.txt");
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
	
	
	counter=0;


//MMAP1
	int fd_mmap;
	printf("Voy a abrir fich\n");
	fd_mmap= open("Datos_Compartidos.txt", O_CREAT|O_TRUNC|O_RDWR, 0666);
	if (fd_mmap < 0) {
           perror("Error creación fichero destino");
           exit(1);
        }
	printf("Voy a escribir en fich\n");
	write(fd_mmap,&datosm,sizeof(datosm));
	mem=static_cast<DatosMemCompartida*> (mmap(NULL,sizeof(datosm),PROT_READ|PROT_WRITE,MAP_SHARED,fd_mmap,0));
	if(mem==MAP_FAILED){
        perror("mmap");
        return;
        }
	
	close(fd_mmap);
//MMAP2
	int fd_mmap2;
	printf("Voy a abrir fich2\n");
	fd_mmap2= open("Datos_Compartidos2.txt", O_CREAT|O_TRUNC|O_RDWR, 0666);
	if (fd_mmap2 < 0) {
           perror("Error creación fichero destino2");
           exit(1);
        }
	printf("Voy a escribir en fich2\n");
	write(fd_mmap2,&datosm2,sizeof(datosm2));
	mem2=static_cast<DatosMemCompartida*> (mmap(NULL,sizeof(datosm2),PROT_READ|PROT_WRITE,MAP_SHARED,fd_mmap2,0));
	if(mem2==MAP_FAILED){
        perror("mmap2");
        return;
        }
	
	close(fd_mmap2);
	
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	//Se dibujan las esferas
	for (i=0; i<esfera.size(); i++)
		esfera[i].Dibuja();

        //Se crea un vector disparo de cada jugador
	for (i=0; i<disparos1.size(); i++)//Vector de disparo para el primer jugador
		disparos1[i].Dibuja();
	for (i=0;i<disparos2.size();i++)//Vector de disparo para el segundo jugador
		disparos2[i].Dibuja();

	
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	int i, j;
	for (i=0;i<esfera.size();i++)//Bucle para mover las esferas
		esfera[i].Mueve(0.025f);
	for(i=0;i<paredes.size();i++)
	{
		for (j=0;j<esfera.size();j++)//Rebote de las esferas con la pared
			paredes[i].Rebota(esfera[j]);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	for (i=0;i<disparos1.size();i++)//Movimiento disparos primer jugador
		disparos1[i].Mueve(0.025f);
	for (i=0;i<disparos2.size();i++)
		disparos2[i].Mueve(0.025f);//Movimiento disparos segundo jugador

	for (i=0;i<esfera.size();i++){
		jugador1.Rebota(esfera[i]);//Rebote de esfera con jugador1
		jugador2.Rebota(esfera[i]);//Rebote de esfera con jugador2
	}

	for(i=0;i<esfera.size();i++){//Rebote de esfera con fondo_izq
		if(fondo_izq.Rebota(esfera[i]))
		{
			disparos1.erase(disparos1.begin(),disparos1.begin()+disparos1.size());	
			disparos2.erase(disparos2.begin(),disparos2.begin()+disparos2.size());	
			esfera[i].centro.x=0;
			esfera[i].centro.y=rand()/(float)RAND_MAX;
			esfera[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
			esfera[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
		}
	}

	for(i=0;i<esfera.size();i++){//Rebote de esfera con fonfo_dcho
		if(fondo_dcho.Rebota(esfera[i]))
		{
			disparos1.erase(disparos1.begin(),disparos1.begin()+disparos1.size());	
			disparos2.erase(disparos2.begin(),disparos2.begin()+disparos2.size());	
			esfera[i].centro.x=0;
			esfera[i].centro.y=rand()/(float)RAND_MAX;
			esfera[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esfera[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;
		}
	}

	
	//Impacto de los disparos del jugador1 contra la raqueta del jugador 2
	for(int i=0;i<disparos1.size();i++)
	{
		if(jugador2.Rebota(disparos1[i]))
		{
			if((jugador2.y2-jugador2.y1)>1.5)
			{
			jugador2.y2=jugador2.y2-0.5;
			jugador2.y1=jugador2.y1+0.5;}
			disparos1.erase(disparos1.begin(),disparos1.begin()+disparos1.size());	
		}
	}
	//Impacto de los disparos del jugador2 contra la raqueta del jugador 1
	for(int i=0;i<disparos2.size();i++)
	{
		if(jugador1.Rebota(disparos2[i]))
		{
			if((jugador1.y2-jugador1.y1)>1.5){
			jugador1.y2=jugador1.y2-0.5;
			jugador1.y1=jugador1.y1+0.5;}
			disparos2.erase(disparos2.begin(),disparos2.begin()+disparos2.size());	
		}
	}

	//Comunicación por sockets
	comunicacion.Receive(mensaje, sizeof(mensaje));
	sscanf(mensaje,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera[0].centro.x,&esfera[0].centro.y,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,&puntos1, &puntos2);
	counter+=25;
	
	if(datosmpunt->accion==-1){OnKeyboardDown('s', 0, 0);}
	else if(datosmpunt->accion==0){}
	else if(datosmpunt->accion==1){OnKeyboardDown('w', 0, 0);}
	
	if(counterr>=10000){
	if(datosmpunt2->accion==-1){count_bot=0;OnKeyboardDown('l', 0, 0);}
	else if(datosmpunt2->accion==0){count_bot=0;}
	else if(datosmpunt2->accion==1){count_bot=0;OnKeyboardDown('o', 0, 0);}
	}
	//Actualizo los datos de la memoria compartida
	datosmpunt->esfera=esfera;
	datosmpunt->raqueta1=jugador1;
	datosmpunt2->esfera=esfera;
	datosmpunt2->raqueta1=jugador2;
	
	
	
}


void CMundo::OnKeyboardDown(unsigned char key, int x, int y)

{
	
	char tecla[]="0";
	
	switch(key)
	{
	case 's':sprintf(tecla,"s");break;
	case 'w':sprintf(tecla,"w"); break;
	case 'l':
		if(flag_nobot==1){
		counter=0;
		}
		sprintf(tecla,"l");break;
	break;
	case 'o':
		if(flag_nobot==1){
		counter=0;
		}
		sprintf(tecla,"o");break;
	case 'e':
	
		sprintf(tecla,"e");break;
	case 'p':
	
		sprintf(tecla,"p");break;
	}
	count_bot=1;
	
	comunicacion.Send(tecla,sizeof(tecla));
}

void CMundo::Init()
{
	//Esfera
	Esfera e;
	e.radio=0.5;
	e.centro.x=0;
	e.centro.y=rand()/(float)RAND_MAX;
	e.velocidad.x=2+2*rand()/(float)RAND_MAX;
	e.velocidad.y=2+2*rand()/(float)RAND_MAX;
	esfera.push_back(e);

	Plano p;

	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

 
	
	//Conexión con servidor
	char ip[]="127.0.0.1";
	char nombre[50];
	printf("¿Cuál es tu nombre ?:\n");
	scanf("%s",nombre); //El cliente pide por teclado un nombre para enviárselo al servidor.
	comunicacion.Connect(ip,8000); // funcion ya definida en sokect.cpp

	//Se envía el nombre al cliente
	comunicacion.Send(nombre,sizeof(nombre));


}


