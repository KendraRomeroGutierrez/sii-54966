// Mundo.cpp: implementation of the CMundo class.
// Autor: Kendra Gabriela Romero Gutierrez
//////////////////////////////////////////////////////////////////////

#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <fcntl.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <iostream>
#include <signal.h>


//Añadimos la función Hilo_Comandos.
void* hilo_comandos1(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}
void* hilo_comandos2(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador2();
}
void* hilo_servidor(void* d)
{
      CMundo* p=(CMundo*) d;
      p->GestionaConexiones();
}

void Recepcion (int n){ 

	if (n==SIGINT) {  
		printf ("El proceso ha fallado por la señal %d \n señal de atencion iterativa (Ctrl +c)\n", n);
		exit (n);}
		
	else if (n==SIGPIPE ){
	
		printf ("El proceso ha fallado por la señal %d \n escritura en un pipe sin lector \n", n);
		exit (n);}
		
	else if (n==SIGTERM )	{
	
		printf ("El proceso ha fallado por la señal %d \n señal por defecto kill \n", n);
		exit (n);
		
	}
	
	else if (n==SIGUSR2){  //El proceso ha ido bien
		printf ("El proceso ha terminado correctamente \n");
                exit (0);
	}
}

void CMundo::RecibeComandosJugador1()
{
	printf("Escuchando teclas jugador 1");
     while (!acabar) {
            usleep(10);
        if(s_client.size()>=2){
            char cad1[100];
            unsigned char key1='\0';
            s_client[0].Receive(cad1,sizeof(cad1));
            sscanf(cad1,"%c",&key1);
         
     switch(key1) {

	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	
	case 'e':
	if(jugador1.getDistancia(disparo1)>=24){
		disparos1.setPos(jugador1.getPos().x, jugador1.getPos().y);
		disparos1.setRadio(0.25);
		disparos1.setVel(6, 0);
	}
		break;
	}
	}
      }
      
}

void CMundo::RecibeComandosJugador2()
{
	printf("Escuchando teclas jugador 2\n");
     while (!acabar) {
            usleep(10);
        if(s_client.size()>=2){
            char cad2[100];
            unsigned char key2='\0';

	 s_client[1].Receive(cad2,sizeof(cad2));
         sscanf(cad2,"%c",&key2);
		//printf("key2: %c\n",key2);
	switch(key2) {
		case 'l':jugador2.velocidad.y=-4;break;
		case 'o':jugador2.velocidad.y=4;break;
	       case 'p':
			if(jugador2.getDistancia(disparos2)>=24){
			disparos2.setPos(jugador2.getPos().x, jugador2.getPos().y);
			disparos2.setRadio(0.25);
			disparos2.setVel(-6, 0);
			}
		break;
	}
      }
      
    }
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
	disparos1.setPos(-300, -300);
	disparos1.setRadio(0);
	disparos1.setVel(0, 0);
	disparos2.setPos(-300, -300);
	disparos2.setRadio(0);
	disparos2.setVel(0, 0);
}

CMundo::~CMundo()
{
	close(tuberia);
	s_client.clear();	
	acabar=1;
	pthread_join(thid1,NULL);
	printf("thid1 vuelve\n");
	pthread_join(thid2,NULL);
	printf("thid2 vuelve\n");
	pthread_join(thid_socket,NULL);
	printf("thid_Socket vuelve\n");
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

        //Se dibujan las esferas.
	for (i=0; i<esfera.size(); i++)
		esfera[i].Dibuja();

        //Se crea un vector disparo de cada jugador
	for (i=0; i<disparos1.size(); i++)//Vector de disparo para el primer jugador
		disparos1[i].Dibuja();
	for (i=0;i<disparos2.size();i++)//Vector de disparo para el segundo jugador
		disparos2[i].Dibuja();

	////////////////////////////////
	//	AQUI TERMINA MI DIBUJO
	///////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	int i,j;

	for (i=0;i<esfera.size();i++)//Bucle para mover las esferas
		esfera[i].Mueve(0.025f);

	for(i=0;i<paredes.size();i++)
	{
		for (j=0;j<esfera.size();j++)//Rebote de las esferas con la pared
			paredes[i].Rebota(esfera[j]);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	for (i=0;i<disparos1.size();i++)//Movimiento disparos primer jugador
		disparos1[i].Mueve(0.025f);
	for (i=0;i<disparos2.size();i++)
		disparos2[i].Mueve(0.025f);//Movimiento disparos segundo jugador
	
	for (i=0;i<esfera.size();i++){
		jugador1.Rebota(esfera[i]);//Rebote de esfera con jugador1
		jugador2.Rebota(esfera[i]);//Rebote de esfera con jugador2
	}

	for(i=0;i<esfera.size();i++){//Rebote de esfera con fondo_izq
		if(fondo_izq.Rebota(esfera[i]))
		{
			disparos1.erase(disparos1.begin(),disparos1.begin()+disparos1.size());	
			disparos2.erase(disparos2.begin(),disparos2.begin()+disparos2.size());	
			esfera[i].centro.x=0;
			esfera[i].centro.y=rand()/(float)RAND_MAX;
			esfera[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
			esfera[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;

			sprintf(msj,"Jugador 1 lleva un total de %d puntos\n", puntos1);
        		int n_traza1 = write(tuberia,msj,100);
			if(n_traza1==-1)
				perror("Error al escribir en Tuberia");
        		sprintf(msj,"Jugador 2 lleva un total de %d puntos\n", puntos2);
        		int n_traza2 = write(tuberia,msj,100);
                        if(n_traza2==-1)
                                perror("Error al escribir en Tuberia");
		}
	}

	for(i=0;i<esfera.size();i++){//Rebote de esfera con fonfo_dcho
		if(fondo_dcho.Rebota(esfera[i]))
		{
			disparos1.erase(disparos1.begin(),disparos1.begin()+disparos1.size());	
			disparos2.erase(disparos2.begin(),disparos2.begin()+disparos2.size());	
			esfera[i].centro.x=0;
			esfera[i].centro.y=rand()/(float)RAND_MAX;
			esfera[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esfera[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;
			
			sprintf(msj,"Jugador 1 lleva un total de %d puntos\n", puntos1);
        		int n_traza1 = write(tuberia,msj,100);
			if(n_traza1==-1)
                                perror("Error al escribir en Tuberia");
        		sprintf(msj,"Jugador 2 lleva un total de %d puntos\n", puntos2);
                        int n_traza2 = write(tuberia,msj,100);
                        if(n_traza2==-1)
                                perror("Error al escribir en Tuberia");

		}
	}

	//Para adaptarnos a la práctica4 no se van a generar más esferas

	//Impacto de los disparos del jugador1 contra la raqueta del jugador 2
	for(int i=0;i<disparos1.size();i++)
	{
		if(jugador2.Rebota(disparos1[i]))
		{
			if((jugador2.y2-jugador2.y1)>1.5)
			{
			jugador2.y2=jugador2.y2-0.5;
			jugador2.y1=jugador2.y1+0.5;}
			disparos1.erase(disparos1.begin(),disparos1.begin()+disparos1.size());	
		}
	}
	//Impacto de los disparos del jugador2 contra la raqueta del jugador 1
	for(int i=0;i<disparos2.size();i++)
	{
		if(jugador1.Rebota(disparos2[i]))
		{
			if((jugador1.y2-jugador1.y1)>1.5){
			jugador1.y2=jugador1.y2-0.5;
			jugador1.y1=jugador1.y1+0.5;}
			disparos2.erase(disparos2.begin(),disparos2.begin()+disparos2.size());	
		}
	}

	//El juego se termina cuando uno de los jugadores llega a 3 puntos
	if ((puntos1 == 3) || (puntos2==3))
		kill(getpid(),SIGUSR2); //Igual que exit(0)

	
	//Comunicación a través de sockets.El servidor envía las coordenadas a través del socket al cliente 
	sprintf(mensaje,"%f %f %f %f %f %f %f %f %f %f %d %d",esfera[0].centro.x,esfera[0].centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1, puntos2);
	comunicacion.Send(mensaje,sizeof(mensaje));
	
	
	for (int i=s_client.size()-1; i>=0; i--) {
         if (s_client[i].Send(mensaje,sizeof(mensaje)) <= 0) {
            s_client.erase(s_client.begin()+i);
            printf("Cliente eliminado\n");
         }
	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	
}

void CMundo::Init()
{
        //Esfera
        Esfera e;
        e.radio=0.5;
        e.centro.x=0;
        e.centro.y=rand()/(float)RAND_MAX;
        e.velocidad.x=2+2*rand()/(float)RAND_MAX;
        e.velocidad.y=2+2*rand()/(float)RAND_MAX;
        esfera.push_back(e);

	Plano p;

	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//Tubería logger.
	tuberia = open("/tmp/Tuberia",O_WRONLY,0777);
	if (tuberia<0)
		perror("Error al abrir tuberia de logger");

	
	//Comunicacion del Socket
	char ip[]="127.0.0.1";	
	if(s.InitServer(ip,4200)==-1)
	printf("error abriendo el servidor\n");
	char nombre[100];
	printf("Esperando Conexion\n");
	s_client.push_back(s.Accept());
	//RECIBIMOS EL NOMBRE DEL CLIENTE
	s_client[s_client.size()-1].Receive(nombre,sizeof(nombre));
	printf("%s se ha conectado a la partida.\n",nombre);
	
	//creamos el thread de los sockets
	pthread_create(&thid_socket, NULL, hilo_servidor, this);
	printf("thread socket\n");	
		
	//creamos el thread
	pthread_create(&thid1, NULL, hilo_comandos1, this);
	printf("thread creado\n");

	//Tratamiento de señales
	struct sigaction act;  // señales
	act.sa_flags=SA_RESTART;  
	act.sa_handler=&Recepcion;  //Apunta a la función
	sigaction(SIGINT, &act, NULL);  // señal de atencion iterativa (ctrl +c)
	sigaction(SIGTERM, &act, NULL); // señal de terminacion por defecto (señal por defecto kill)
	sigaction(SIGPIPE, &act, NULL); // escritura en un pipe sin lectores
	sigaction(SIGUSR2, &act, NULL); // señal definida por la aplicacion
}

void* hilo_servidor(void* d)
{
      CMundo* p=(CMundo*) d;
      p->GestionaConexiones();
}

void CMundo::GestionaConexiones(){
	while(!acabar){
		Socket s_communication;
		char nombre[100];
		printf("Esperando Conexion\n");
		s_communication=s.Accept();
		s_client.push_back(s_communication);
		//RECIBIMOS EL NOMBRE DEL CLIENTE
		s_client[s_client.size()-1].Receive(nombre,sizeof(nombre));
		printf("%s se ha conectado a la partida.\n",nombre);
		if(s_client.size()==2)
		pthread_create(&thid2, NULL, hilo_comandos2, this);
	}
}

