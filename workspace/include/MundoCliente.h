
//  Autor: Kendra Gabriela Romero Gutierrez
// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "DatosMemCompartida.h"
#include "Socket.h"
#include <pthread.h>

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	DatosMemCompartida datosm;
	DatosMemCompartida* datosmpunt;
	DatosMemCompartida datosm2;
	DatosMemCompartida* datosmpunt2;

//	Esfera esfera;
	std::vector<Esfera> esfera;
	std::vector<Esfera> disparos1;
	std::vector<Esfera> disparos2;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	int counter;
	int count_bot=1;


	// Declaración del socket para la comunicacion 
	char mensaje[120];
	char mensaje2[120];  // opcional
	Socket comunicacion ;// P5 
	// envio de teclas
	void SendComandosJugador();
	pthread_attr_t attr;
	pthread_t thid1;
	
	
	
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)


