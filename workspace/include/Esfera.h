// Autora: Kendra Gabriela Romero Gutierrez
// Esfera.h: interface for the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ESFERA_H_8D520BAF_8208_423B_BD91_29F6687FB9C3INCLUDED)
#define AFX_ESFERA_H_8D520BAF_8208_423B_BD91_29F6687FB9C3INCLUDED


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"

class Esfera  
{
public:	
	Esfera();
	virtual ~Esfera();
		
	Vector2D centro;
	Vector2D velocidad;
	float radio;
	unsigned char azul;
	unsigned char verde;
	unsigned char rojo;
	void Mueve(float t);
	void Dibuja();
};

#endif // !defined(AFX_ESFERA_H_8D520BAF_8208_423B_BD91_29F6687FB9C3INCLUDED)
