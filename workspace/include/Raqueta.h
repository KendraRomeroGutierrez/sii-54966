// Raqueta.h: interface for the Raqueta class.
//  Autor: Kendra Gabriela Romero Gutierrez
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;
	Vector2D aceleracion;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
