
//  Autor: Kendra Gabriela Romero Gutierrez
// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////



#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include <pthread.h>
#include "Socket.h"

class CMundo
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();
	void InitGL();
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	//void RecibeComandosJugador();

//	Esfera esfera;
	std::vector<Esfera> esfera;
	std::vector<Esfera> disparos1;
	std::vector<Esfera> disparos2;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	


	//Tubería logger.
	int tuberia;
	char msj[100];
	
	
	//hilo 
	pthread_t thid1;
	
	char mensaje[120];
	
	// Declaración del socket para la conexion 
	Socket conexion;
	
	// Declaración del socket para la comunicacion 
	Socket comunicacion;
	std::vector<Socket> s_client;
  	 void GestionaConexiones();
  	 int acabar=0;

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)

